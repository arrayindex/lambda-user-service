# README #

### What is this repository for? ###

* A simple example of a Java Amazon Lambda Function, as a first crack at Amazon lambda.

### How do I get set up? ###

* Checkout the application code
* Use Maven to pull down the dependencies through your favorite IDE 
* I use IntelliJ
* Execute mvn package (this will execute all of the unit tests with their mocks and output a Jar that can be uploaded into Amazon Lambda)

### Runtime ###
* Application executes off of environment variables that can be set in Amazon Lambda UI
* The ApplicationProperties class handles this complexity of reading from environment
* This class defaults to safe local values for local development and testing
* If a table does not exist in dynamodb then it will be automatically created via the application very similar to the mogodb pattern

1. **service_popular_cap** allows configuration of the friend count required to become popular.
2. **facebook_application_secret** pulls the application secret key ***note*** this should be encrypted by Amazon Encryption helper
3. **core_dynamo_connection_string** sets our dynamo connection string example http://localhost:8000
4. **core_dynamo_region** set the dynamo region

### Deployment ###
* Uploading a JAR to the Lambda Function UI or via S3 buckets seems to be most effective.
* A Jenkins job that listens off a trigger against a github/bitbucket hook could also kick this off automatically

### Runtime ###
* Intended Swagger Definition can be found here https://swaggerhub.com/apis/Arrayindex/Hello_world/1.0.0

### Testing ###
* Application can be tested by standard JUnit testing tools
* There is an Integration test that takes in a real auth token and connects to the local dynamodb instance to allow for full on tests as well
* Maven package command also will execute the unit tests that were created with the package.


### Notes ###
* Application is built using dependency injection to allow for easy testing and mocking of interfaces (DynamoDB mocks and RestFB)
* Business logic is extracted from the request interface so if the home of the application moves from Amazon Lambda to Azure/Google/In house the migration can be a quick one.
* Performance of UI was taken into account and API calls were broken up to make things easier for the UI to handle async.


![Capture.PNG](https://bitbucket.org/repo/BgBBdr8/images/3577505392-Capture.PNG)