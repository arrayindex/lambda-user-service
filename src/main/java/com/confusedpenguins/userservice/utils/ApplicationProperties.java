package com.confusedpenguins.userservice.utils;

/**
 * Created by Andrew on 7/8/2017.
 */
public class ApplicationProperties {
    //defaults are for easy local development only. In production lambda these should be set
    private static final String defaultFacebookAppSecret = "3f75147fe57eae0373679213c3e59235";
    private static final String defaultPopularityCap = "50";
    private static final String defaultDynamoDBConfiguration = "http://localhost:8000";
    private static final String defaultDynamoDBRegion = "us-east-1";

    public static String getDefaultFacebookAppSecret() {
        String applicationId = System.getenv("facebook_application_secret");
        return (applicationId != null) ? applicationId : defaultFacebookAppSecret;
    }

    public static int getPopularCap() {
        String maxFriends = System.getenv("service_popular_cap");
        if (maxFriends == null) {
            maxFriends = defaultPopularityCap;
        }
        return Integer.parseInt(maxFriends);
    }

    public static String getDynamoDBConnectionString() {
        String connectionString = System.getenv("core_dynamo_connection_string");
        if (connectionString == null) {
            connectionString = defaultDynamoDBConfiguration;
        }
        return connectionString;
    }

    public static String getDynamoDBRegion() {
        String connectionString = System.getenv("core_dynamo_region");
        if (connectionString == null) {
            connectionString = defaultDynamoDBRegion;
        }
        return connectionString;
    }
}