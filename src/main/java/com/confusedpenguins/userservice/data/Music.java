package com.confusedpenguins.userservice.data;

import java.util.ArrayList;

/**
 * Created by Andrew on 7/13/2017.
 */
public class Music {
    private Data[] data;
    private Paging paging;

    public Data[] getData() {
        return data;
    }

    public void setData(Data[] data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    @Override
    public String toString() {
        return "Music{" +
                "data=" + data +
                '}';
    }
}
