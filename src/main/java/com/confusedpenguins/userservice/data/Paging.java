package com.confusedpenguins.userservice.data;

/**
 * Created by Andrew on 7/13/2017.
 */
public class Paging {
    String next;

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }
}
