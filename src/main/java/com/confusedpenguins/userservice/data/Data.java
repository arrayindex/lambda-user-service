package com.confusedpenguins.userservice.data;
/**
 * Created by Andrew on 7/13/2017.
 */
public class Data {

    private String name;

    private String id;

    private int fan_count;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getFan_count() {
        return fan_count;
    }

    public void setFan_count(int fan_count) {
        this.fan_count = fan_count;
    }

    @Override
    public String toString() {
        return "Data{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", fan_count=" + fan_count +
                '}';
    }
}