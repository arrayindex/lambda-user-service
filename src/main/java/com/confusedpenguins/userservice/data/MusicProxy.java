package com.confusedpenguins.userservice.data;

import java.util.ArrayList;

/**
 * Created by Andrew on 7/12/2017.
 */
public class MusicProxy {
    private Music music;

    public Music getMusic() {
        return music;
    }

    public void setMusic(Music music) {
        this.music = music;
    }

    public Data[] getData() {
        if (this.getMusic() != null) {
            return this.getMusic().getData();
        }
        Data[] empty = {};
        return empty;
    }

    @Override
    public String toString() {
        return "MusicProxy{" +
                "music=" + music +
                '}';
    }
}


