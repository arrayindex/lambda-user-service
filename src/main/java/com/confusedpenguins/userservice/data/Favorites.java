package com.confusedpenguins.userservice.data;

/**
 * Created by Andrew on 7/10/2017.
 */
public class Favorites {
    private String favoriteLocation;
    private String favoriteBand;

    public boolean validate() {
        return (this.favoriteLocation != null);
    }

    public String getFavoriteLocation() {
        return favoriteLocation;
    }

    public void setFavoriteLocation(String favoriteLocation) {
        this.favoriteLocation = favoriteLocation;
    }

    public String getFavoriteBand() {
        return favoriteBand;
    }

    public void setFavoriteBand(String favoriteBand) {
        this.favoriteBand = favoriteBand;
    }

    public Favorites(String favoriteLocation, String favoriteBand) {
        this.favoriteLocation = favoriteLocation;
        this.favoriteBand = favoriteBand;
    }
}
