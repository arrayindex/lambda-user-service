package com.confusedpenguins.userservice.workers;

import com.confusedpenguins.userservice.data.Data;
import com.confusedpenguins.userservice.data.MusicProxy;
import com.confusedpenguins.userservice.utils.ApplicationProperties;
import com.google.gson.Gson;
import com.restfb.*;
import com.restfb.exception.FacebookOAuthException;

/**
 * Created by Andrew on 7/12/2017.
 */
public class MusicService {
    private SocialManager socialManager;
    private FacebookClient facebookClient;
    private Gson gson = new Gson();

//    public static void main(String[] args) {
//        String authToken = "EAAQdzGvlYm8BABWZCqkw8lNBrnra7cmMHf0c6HZA7rss2Y8eg547aYHMHsf4ZBfge0f9hN6MT51vO3QJebBbPOuL0j3zGtolG08QZAIKuiVKnEU77kAhx0uCzyP2OG5dq2edfRdVAZCZC5kgA2GGM8DjhJvjG7FjsbuHWIdf8aPbER7aza9WDNpULxKymJObMZD";
//        FacebookClient facebookClient = new DefaultFacebookClient(authToken, ApplicationProperties.getDefaultFacebookAppSecret(), Version.VERSION_2_8);
//        MusicService ms = new MusicService(facebookClient);
//        System.out.println(ms.getFavoriteBand());
//    }

    public MusicService(FacebookClient facebookClient) {
        this.facebookClient = facebookClient;
        this.socialManager = new SocialManager();
    }

    public String getFavoriteBand() throws FacebookOAuthException {
        String rawOut = facebookClient.fetchObject("me", String.class, Parameter.with("fields", "music{fan_count,name}"));

        if (gson.fromJson(rawOut, MusicProxy.class) == null) {
            return "Unable to retrieve Bands";
        } else {
            String nextUrl = gson.fromJson(rawOut, MusicProxy.class).getMusic().getPaging().getNext();


            processPage(gson.fromJson(rawOut, MusicProxy.class).getMusic().getData());
            if (nextUrl != null) {
                Connection<String> myFeed;
                do {
                    myFeed = facebookClient.fetchConnectionPage(nextUrl, String.class);
                    processPage(gson.fromJson(myFeed.getData().toString(), Data[].class));
                    nextUrl = myFeed.getNextPageUrl();
                } while (myFeed.hasNext());
            }
            return this.socialManager.getFavoriteThing();
        }
    }

    private void processPage(Data[] myFeed) {
        for (Data music : myFeed) {
            socialManager.analyzeDataPoint(music);
        }
    }
}
