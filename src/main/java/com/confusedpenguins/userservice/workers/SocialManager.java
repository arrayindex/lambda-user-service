package com.confusedpenguins.userservice.workers;

import com.confusedpenguins.userservice.data.Data;
import com.confusedpenguins.userservice.utils.ApplicationProperties;

import java.util.HashMap;

/**
 * Created by Andrew on 7/8/2017.
 */
public class SocialManager {
    private HashMap<String, Integer> likeMap;

    public void analyzeDataPoint(String rawString) {
        if (rawString != null) {
            rawString = rawString.trim();
            int count = likeMap.containsKey(rawString) ? likeMap.get(rawString) : 0;
            likeMap.put(rawString, count + 1);
        }
    }

    public void analyzeDataPoint(Data band) {
        if (band != null) {
            int count = band.getFan_count();
            likeMap.put(band.getName(), count);
        }
    }

    public SocialManager() {
        this.likeMap = new HashMap<>();
    }

    public static boolean isPopular(long friendCount) {
        int popularityIndex = ApplicationProperties.getPopularCap();
        return (friendCount >= popularityIndex);
    }

    public String getFavoriteThing() {
        if (likeMap != null) {
            int max = 0;
            String maxGenre = "";
            for (String key : likeMap.keySet()) {
                if (likeMap.get(key) > max) {
                    max = likeMap.get(key);
                    maxGenre = key;
                }
            }
            return maxGenre;
        } else {
            return null;
        }
    }
}
