package com.confusedpenguins.userservice.workers;

import com.confusedpenguins.userservice.data.UserProxy;
import com.restfb.*;
import com.restfb.exception.FacebookOAuthException;
import com.restfb.types.*;

/**
 * Created by Andrew on 7/8/2017.
 */
public class LoginService {
//    public static void main(String[] args) {
//        String authToken = "EAAQdzGvlYm8BAPBtdfPWZAoz6RzfMJuZA2OQZB4JDqf0X8aHojRmCtADweGx72ZCRbbrZCMahZA7PWdKHXmlgdZAMGjUrH4edwKvQ6PHY4HQdm4CvQViZCHSqPi1kBEvwR5hJMH9wNx9nYMZApjGtHiZAIiqFFfWIazILbKRqZCZANFqQVAx2YValFnSqSyn7Sa8iCYZD";
//        try {
//            FacebookClient facebookClient = new DefaultFacebookClient(authToken, ApplicationProperties.getDefaultFacebookAppSecret(), Version.VERSION_2_8);
//            UserProxy user = new LoginService(facebookClient).getProfile();
//            System.out.println(user.toJsonString());
//            System.out.println(user.isPopular());
//        } catch (FacebookOAuthException e) {
//            System.out.println(e);
//            e.printStackTrace();
//        }
//    }

    FacebookClient facebookClient;

    public LoginService(FacebookClient facebookClient) {
        this.facebookClient = facebookClient;
    }

    public long getFriendCount() throws FacebookOAuthException {
        Connection<User> myFriends = facebookClient.fetchConnection("me/friends", User.class);
        try {
            if (myFriends != null) {
                return myFriends.getTotalCount();
            } else {
                return 0;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public UserProxy getProfile() throws FacebookOAuthException {
        if (facebookClient != null) {
            User facebookUserResponse = facebookClient.fetchObject("me", User.class, Parameter.with("fields", "id,hometown,picture,about,email,first_name,last_name"));
            UserProxy user = proxyProfile(facebookUserResponse);
            return user;
        }
        return null;
    }

    private UserProxy proxyProfile(User facebookUserResponse) {
        if (facebookUserResponse != null) {
            UserProxy userProxy = new UserProxy();
            userProxy.setId(facebookUserResponse.getId());
            userProxy.setFirstName(facebookUserResponse.getFirstName());
            userProxy.setLastName(facebookUserResponse.getLastName());
            userProxy.setEmail(facebookUserResponse.getEmail());
            if (facebookUserResponse.getPicture() != null) {
                userProxy.setPictureUrl(facebookUserResponse.getPicture().getUrl());
            } else {
                userProxy.setPictureUrl("myDefaultImage");
            }

            userProxy.setHometown(facebookUserResponse.getHometownName());
            return userProxy;
        } else {
            return null;
        }
    }
}