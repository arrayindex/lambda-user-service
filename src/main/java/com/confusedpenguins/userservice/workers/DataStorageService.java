package com.confusedpenguins.userservice.workers;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import com.confusedpenguins.userservice.data.UserProxy;
import com.confusedpenguins.userservice.interfaces.database.ObjectHelperInterface;

import java.util.ArrayList;
import java.util.List;

import static com.confusedpenguins.userservice.data.UserProxy.tableName;

/**
 * Created by Andrew on 7/12/2017.
 */
public class DataStorageService implements ObjectHelperInterface<UserProxy> {
    AmazonDynamoDB dynamoDBClient;

    public DataStorageService(AmazonDynamoDB dynamoDBClient) {
        this.dynamoDBClient = dynamoDBClient;
    }

    /**
     * This method will save a user object into the User table, if the table doesn't previously exist it will create it automatically.
     *
     * @param user
     * @return
     */
    @Override
    public UserProxy saveObject(UserProxy user) {
        if (!doesTableExist(this.dynamoDBClient, UserProxy.tableName)) {
            try {
                List<KeySchemaElement> elements = new ArrayList<>();
                KeySchemaElement keySchemaElement = new KeySchemaElement()
                        .withKeyType(KeyType.HASH)
                        .withAttributeName(UserProxy.primaryKey);
                elements.add(keySchemaElement);
                List<AttributeDefinition> attributeDefinitions = new ArrayList<>();
                attributeDefinitions.add(new AttributeDefinition()
                        .withAttributeName(UserProxy.primaryKey)
                        .withAttributeType(ScalarAttributeType.S));
                CreateTableRequest createTableRequest = new CreateTableRequest()
                        .withTableName(UserProxy.tableName)
                        .withKeySchema(elements)
                        .withProvisionedThroughput(new ProvisionedThroughput()
                                .withReadCapacityUnits(5L)
                                .withWriteCapacityUnits(5L))
                        .withAttributeDefinitions(attributeDefinitions);
                dynamoDBClient.createTable(createTableRequest);
                System.out.println("Waiting for " + tableName + " to be created...this may take a while...");
            } catch (Exception e) {
                System.err.println("CreateTable request failed for " + tableName);
                System.err.println(e.getMessage());
            }
        }
        DynamoDBMapper mapper = new DynamoDBMapper(this.dynamoDBClient);
        mapper.save(user);
        return user;
    }

    public static boolean doesTableExist(AmazonDynamoDB dynamo, String tableName) {
        try {
            TableDescription table = dynamo.describeTable(new DescribeTableRequest(tableName))
                    .getTable();
            return TableStatus.ACTIVE.toString().equals(table.getTableStatus());
        } catch (ResourceNotFoundException rnfe) {
            // This means the table doesn't exist in the account yet
            return false;
        }
    }

    private boolean userTableExists(AmazonDynamoDB dynamoDBClient, String tableName) {
        try {
            TableDescription table = dynamoDBClient.describeTable(new DescribeTableRequest(tableName))
                    .getTable();
            return TableStatus.ACTIVE.toString().equals(table.getTableStatus());
        } catch (ResourceNotFoundException rnfe) {
            return false;
        }
    }

    /**
     * FIND ALL THE THINGS
     *
     * @param query
     * @return
     */
    @Override
    public List<UserProxy> find(UserProxy query) {
        if (query != null) {
            DynamoDBMapper mapper = new DynamoDBMapper(this.dynamoDBClient);
            DynamoDBQueryExpression<UserProxy> queryExpression = new DynamoDBQueryExpression<UserProxy>().withHashKeyValues(query);
            List<UserProxy> itemList = mapper.query(UserProxy.class, queryExpression);
            return itemList;
        }
        return null;
    }
}
