package com.confusedpenguins.userservice.workers;

import com.restfb.Connection;
import com.restfb.FacebookClient;
import com.restfb.exception.FacebookOAuthException;
import com.restfb.types.Post;

import java.util.Collection;

/**
 * Created by Andrew on 7/8/2017.
 */
public class LocationService {
    private SocialManager socialManager;
    private FacebookClient facebookClient;

    public LocationService(FacebookClient facebookClient) {
        this.facebookClient = facebookClient;
        this.socialManager = new SocialManager();

    }

//    public static void main(String[] args) {
//        LocationService service = new LocationService();
//        String authToken = "EAAQdzGvlYm8BALfNINOheMiakSnp40Gr26rc8OLn8mvTn9hwU5ZAQcSdWEgNz4Km3wfAle8ZCcujFULoN0KxayZA9wmXXHCVFcI5Dwq2NkuCJRnXUriaNmgzJu7EN3t0TcvZAa5AbZAalZB21bF8FUMVYgWZCwBVCP1d2Sj4RXkOQv8ZAZAktRf5fvb3StZB6VsBcZD";
//        String test = service.getFavoriteLocation(authToken);
//        System.out.println(test);
//    }


    public String getFavoriteLocation() throws FacebookOAuthException {
        this.socialManager = new SocialManager();
        Connection<Post> myFeed;
        myFeed = facebookClient.fetchConnection("me/tagged_places", Post.class);
        processPage(myFeed.getData());
        do {
            myFeed = facebookClient.fetchConnectionPage(myFeed.getNextPageUrl(), Post.class);
            processPage(myFeed.getData());
        } while (myFeed.hasNext());
        return this.socialManager.getFavoriteThing();
    }

    private void processPage(Collection<Post> datas) {
        for (Post post : datas) {
            if (post.getPlace() != null) {
                this.socialManager.analyzeDataPoint(post.getPlace().getName());
            }
        }
    }
}
