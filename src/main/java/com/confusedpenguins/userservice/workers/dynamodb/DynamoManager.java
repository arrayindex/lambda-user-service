package com.confusedpenguins.userservice.workers.dynamodb;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.confusedpenguins.userservice.utils.ApplicationProperties;

/**
 * Created by Andrew on 7/10/2017.
 */
public class DynamoManager {
    private static DynamoManager adapter = new DynamoManager();

    private final AmazonDynamoDB client;

    private DynamoManager() {
        client = AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration(ApplicationProperties.getDynamoDBConnectionString(), ApplicationProperties.getDynamoDBRegion()))
                .build();
    }

    public static DynamoManager getInstance() {
        if (adapter == null) {
            adapter = new DynamoManager();
        }
        return adapter;
    }

    public AmazonDynamoDB getClient() {
        return this.client;
    }
}
