package com.confusedpenguins.userservice.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.confusedpenguins.userservice.data.Favorites;
import com.confusedpenguins.userservice.handlers.rest.UserServiceError;
import com.confusedpenguins.userservice.handlers.rest.UserServiceResponse;
import com.confusedpenguins.userservice.utils.ApplicationProperties;
import com.confusedpenguins.userservice.workers.LocationService;
import com.confusedpenguins.userservice.handlers.rest.UserServiceLoginRequest;
import com.confusedpenguins.userservice.workers.MusicService;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;
import com.restfb.exception.FacebookOAuthException;

import java.util.HashMap;

/**
 * This is the class that pulls in a user's favorites information,
 * Since these calls can be less than performant (few seconds) due to api paging
 * this service was broken up to allow a front end service to call it async at it's leisure to allow for best performance
 * in the mobile/web ui.
 *
 * This call pulls the user's favorite location and favorite (most popular band)
 * Created by Andrew on 7/8/2017.
 */
public class UserFavoritesHandler implements RequestHandler<UserServiceLoginRequest, UserServiceResponse> {
    @Override
    public UserServiceResponse handleRequest(UserServiceLoginRequest userServiceLoginRequest, Context context) {
        if (userServiceLoginRequest.getHeaders() == null) {
            return null;
        } else if (userServiceLoginRequest.getHeaders().containsKey("X-facebook-authtoken")) {
            try {
                String authToken = userServiceLoginRequest.getHeaders().get("X-facebook-authtoken");
                FacebookClient facebookClient = new DefaultFacebookClient(authToken, ApplicationProperties.getDefaultFacebookAppSecret(), Version.VERSION_2_8);
                return invokeRequest(facebookClient);
            } catch (Exception exception) {
                UserServiceError error = new UserServiceError(exception.getMessage());
                return new UserServiceResponse(error, new HashMap<String, String>(), 500);
            }
        } else {
            UserServiceError error = new UserServiceError("Missing Login Token");
            return new UserServiceResponse(error, new HashMap<String, String>(), 401);
        }
    }

    public UserServiceResponse invokeRequest(FacebookClient facebookClient) {
        try {
            LocationService locationService = new LocationService(facebookClient);
            MusicService musicService = new MusicService(facebookClient);

            String favoriteLocation = locationService.getFavoriteLocation();
            String favoritBand = musicService.getFavoriteBand();


            Favorites favoriteLocationObject = new Favorites(favoriteLocation, favoritBand);
            if (favoriteLocationObject.validate()) {
                return new UserServiceResponse(favoriteLocationObject, new HashMap<String, String>(), 200);
            } else {
                return new UserServiceResponse(favoriteLocationObject, new HashMap<String, String>(), 500);
            }
        } catch (FacebookOAuthException exception) {
            UserServiceError error = new UserServiceError(exception.getMessage());
            return new UserServiceResponse(error, new HashMap<String, String>(), 401);
        }
    }
}
