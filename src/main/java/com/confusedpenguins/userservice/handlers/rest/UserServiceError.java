package com.confusedpenguins.userservice.handlers.rest;

/**
 * Created by Andrew on 7/9/2017.
 */
public class UserServiceError {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserServiceError(String message) {
        this.message = message;
    }
}
