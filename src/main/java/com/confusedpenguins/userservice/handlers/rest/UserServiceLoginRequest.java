package com.confusedpenguins.userservice.handlers.rest;

import java.util.Map;

/**
 * Created by Andrew on 7/8/2017.
 */
public class UserServiceLoginRequest {
    private Map<String, String> headers;

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
