package com.confusedpenguins.userservice.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.confusedpenguins.userservice.data.UserProxy;
import com.confusedpenguins.userservice.workers.DataStorageService;
import com.confusedpenguins.userservice.workers.dynamodb.DynamoManager;
import com.confusedpenguins.userservice.handlers.rest.UserServiceError;
import com.confusedpenguins.userservice.utils.ApplicationProperties;
import com.confusedpenguins.userservice.workers.LoginService;
import com.confusedpenguins.userservice.handlers.rest.UserServiceLoginRequest;
import com.confusedpenguins.userservice.handlers.rest.UserServiceResponse;
import com.confusedpenguins.userservice.workers.SocialManager;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;
import com.restfb.exception.FacebookOAuthException;

import java.util.Date;
import java.util.HashMap;

/**
 * User Auth Handler, This is the function that will recieve requests from Amazon Lambda Function
 * This will pull the user's profile information from facebook, ser whether they are popular and save the user into
 * dynamoDb in one call.
 * Created by Andrew on 7/8/2017.
 */
public class UserAuthHandler implements RequestHandler<UserServiceLoginRequest, UserServiceResponse> {
    @Override
    public UserServiceResponse handleRequest(UserServiceLoginRequest userServiceLoginRequest, Context context) {
        if (userServiceLoginRequest.getHeaders() == null) {
            UserServiceError error = new UserServiceError("Missing Headers");
            return new UserServiceResponse(error, new HashMap<String, String>(), 500);
        } else if (userServiceLoginRequest.getHeaders().containsKey("X-facebook-authtoken")) {
            String authToken = userServiceLoginRequest.getHeaders().get("X-facebook-authtoken");
            FacebookClient facebookClient = new DefaultFacebookClient(authToken, ApplicationProperties.getDefaultFacebookAppSecret(), Version.VERSION_2_8);
            DataStorageService dataStorageService = new DataStorageService(DynamoManager.getInstance().getClient());

            return invokeRequest(facebookClient, dataStorageService);
        } else {
            UserServiceError error = new UserServiceError("Missing Login Token");
            return new UserServiceResponse(error, new HashMap<String, String>(), 401);
        }
    }

    public UserServiceResponse invokeRequest(FacebookClient facebookClient, DataStorageService dataStorageService) {
        LoginService loginService = new LoginService(facebookClient);
        try {
            UserProxy user = loginService.getProfile();
            user.setLastLogin(new Date());
            user.setPopular(SocialManager.isPopular(loginService.getFriendCount()));

            dataStorageService.saveObject(user);
            HashMap<String, String> responseHeaders = new HashMap<>();
            responseHeaders.put("X-plynk-userid", user.getId());

            return new UserServiceResponse(user, responseHeaders, 200);
        } catch (FacebookOAuthException exception) {
            UserServiceError error = new UserServiceError(exception.getMessage());
            return new UserServiceResponse(error, new HashMap<String, String>(), 401);
        }
    }
}
