package com.confusedpenguins.userservice.interfaces.database;


import java.util.List;

/**
 * Created by Andrew on 7/10/2017.
 */
public interface ObjectHelperInterface<T> {
    T saveObject(T object);

    List<T> find(T query);
}
