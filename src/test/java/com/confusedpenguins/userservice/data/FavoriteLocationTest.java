package com.confusedpenguins.userservice.data;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 7/12/2017.
 * <p>
 * Initially this seems like overkill but validation could become more intense as time goes on.
 */
public class FavoriteLocationTest {

    @Test
    public void validate() throws Exception {
        Favorites shouldBeInvalid = new Favorites(null, null);
        Favorites shouldBeValid = new Favorites("Any Pub", null);

        assertEquals("Favorite Location should be invalid if null", false, shouldBeInvalid.validate());
        assertEquals("Favorite Location should be valid if not null", true, shouldBeValid.validate());
    }

}