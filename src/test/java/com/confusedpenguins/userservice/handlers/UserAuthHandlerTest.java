package com.confusedpenguins.userservice.handlers;

import com.confusedpenguins.userservice.handlers.rest.UserServiceResponse;
import com.confusedpenguins.userservice.workers.DataStorageService;
import com.confusedpenguins.userservice.workers.dynamodb.DynamoManager;
import com.confusedpenguins.userservice.workers.mocks.DataStorageServiceMock;
import com.confusedpenguins.userservice.workers.mocks.FacebookClientMock;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 7/12/2017.
 */
public class UserAuthHandlerTest {
    @Test
    public void invokeRequest() throws Exception {
        UserAuthHandler authHandler = new UserAuthHandler();
        UserServiceResponse response = authHandler.invokeRequest(new FacebookClientMock(), new DataStorageServiceMock(DynamoManager.getInstance().getClient()));
        assertNotEquals(null,response);
    }
}