package com.confusedpenguins.userservice.workers;

import com.confusedpenguins.userservice.data.UserProxy;
import com.confusedpenguins.userservice.workers.mocks.FacebookClientMock;
import org.junit.Test;
import org.powermock.core.classloader.annotations.PrepareForTest;


import static org.junit.Assert.*;

/**
 * Created by Andrew on 7/11/2017.
 */
@PrepareForTest(LoginService.class)
public class LoginServiceTest {
    LoginService loginServiceTest;

    @Test
    public void getProfile() throws Exception {
        loginServiceTest = new LoginService(new FacebookClientMock());
        UserProxy actual = loginServiceTest.getProfile();
        assertNotEquals("getProfile should not return null", null, actual);
    }
}