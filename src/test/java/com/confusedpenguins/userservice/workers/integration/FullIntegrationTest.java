package com.confusedpenguins.userservice.workers.integration;

import com.confusedpenguins.userservice.handlers.UserAuthHandler;
import com.confusedpenguins.userservice.handlers.rest.UserServiceResponse;
import com.confusedpenguins.userservice.utils.ApplicationProperties;
import com.confusedpenguins.userservice.workers.DataStorageService;
import com.confusedpenguins.userservice.workers.LocationService;
import com.confusedpenguins.userservice.workers.MusicService;
import com.confusedpenguins.userservice.workers.SocialManager;
import com.confusedpenguins.userservice.workers.dynamodb.DynamoManager;
import com.confusedpenguins.userservice.workers.mocks.DataStorageServiceMock;
import com.confusedpenguins.userservice.workers.mocks.FacebookClientMock;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Version;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Andrew on 7/13/2017.
 */
public class FullIntegrationTest {
    String authToken = "";
    FacebookClient facebookClient;

    @Before
    public void initialize() {
        authToken = "EAAQdzGvlYm8BAF3D3G7hA2GO3LVKKZBtSfB4WOc79COkjFJUqss8BFasHNC9HSvrjFdI4BtMGT6lxwhRNk4SFWWVXOeO8oOOko8mmWVmhC9QkJJNgqsN8ZAG9ZAyFOz57Uz0DjlJofcXCZACNZASZBJwZA02vsTcZBFtvdNqoMj4DamwEfzRugH6ZCpV7ki9eDZAQZD";
        facebookClient = new DefaultFacebookClient(authToken, ApplicationProperties.getDefaultFacebookAppSecret(), Version.VERSION_2_8);
    }

    @Test
    public void getFavoriteLocation() throws Exception {
        LocationService locationService = new LocationService(facebookClient);
        assertEquals("Dublin, Ireland is best", "Dublin, Ireland", locationService.getFavoriteLocation());
    }

    @Test
    public void getFavoriteBand() throws Exception {
        MusicService ms = new MusicService(facebookClient);
        assertEquals("Metallica is best", "Metallica", ms.getFavoriteBand());
    }

    @Test
    public void invokeRequest() throws Exception {
        UserAuthHandler authHandler = new UserAuthHandler();
        UserServiceResponse response = authHandler.invokeRequest(facebookClient, new DataStorageService(DynamoManager.getInstance().getClient()));
        assertNotEquals("Get Me Something", null, response.getBody());
    }
}
