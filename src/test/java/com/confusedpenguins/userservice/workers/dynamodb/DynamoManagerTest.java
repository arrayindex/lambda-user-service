package com.confusedpenguins.userservice.workers.dynamodb;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 7/12/2017.
 */
public class DynamoManagerTest {
    @Test
    public void getClientTest() throws Exception {
        assertNotEquals("Should not be null", null, DynamoManager.getInstance().getClient());
    }
}