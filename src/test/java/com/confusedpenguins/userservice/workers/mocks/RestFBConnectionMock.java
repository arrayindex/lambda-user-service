package com.confusedpenguins.userservice.workers.mocks;

import com.restfb.Connection;
import com.restfb.FacebookClient;
import com.restfb.exception.FacebookJsonMappingException;
import com.restfb.json.JsonArray;
import com.restfb.json.JsonException;
import com.restfb.json.JsonObject;
import com.restfb.types.User;

import static java.util.Collections.unmodifiableList;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Andrew on 7/12/2017.
 */
public class RestFBConnectionMock<T> extends Connection<T> {
    private FacebookClient facebookClient;
    private Class<T> connectionType;
    private List<T> data;
    private String previousPageUrl;
    private String nextPageUrl;
    private Long totalCount;
    private String beforeCursor;
    private String afterCursor;

    /**
     * Creates a connection with the given {@code jsonObject}.
     *
     * @param facebookClient The {@code FacebookClient} used to fetch additional pages and map data to JSON objects.
     * @param json           Raw JSON which must include a {@code data} field that holds a JSON array and optionally a {@code paging}
     *                       field that holds a JSON object with next/previous page URLs.
     * @param connectionType Connection type token.
     * @throws FacebookJsonMappingException If the provided {@code json} is invalid.
     * @since 1.6.7
     */
    @SuppressWarnings("unchecked")
    public RestFBConnectionMock(FacebookClient facebookClient, String json, Class<T> connectionType) {
        super(facebookClient, json, connectionType);
        List<T> dataList = new ArrayList<T>();

        if (json == null) {
            throw new FacebookJsonMappingException("You must supply non-null connection JSON.");
        }

        JsonObject jsonObject;

        try {
            jsonObject = new JsonObject(json);
        } catch (JsonException e) {
            throw new FacebookJsonMappingException("The connection JSON you provided was invalid: " + json, e);
        }

        // Pull out data
        JsonArray jsonData = jsonObject.getJsonArray("data");
        for (int i = 0; i < jsonData.length(); i++) {
            dataList.add(connectionType.equals(JsonObject.class) ? (T) jsonData.get(i)
                    : facebookClient.getJsonMapper().toJavaObject(jsonData.get(i).toString(), connectionType));
        }

        // Pull out paging info, if present
        if (jsonObject.has("paging")) {
            JsonObject jsonPaging = jsonObject.getJsonObject("paging");
            previousPageUrl = jsonPaging.has("previous") ? jsonPaging.getString("previous") : null;
            nextPageUrl = jsonPaging.has("next") ? jsonPaging.getString("next") : null;
            if (null != previousPageUrl && previousPageUrl.startsWith("http://")) {
                previousPageUrl = previousPageUrl.replaceFirst("http://", "https://");
            }
            if (null != nextPageUrl && nextPageUrl.startsWith("http://")) {
                nextPageUrl = nextPageUrl.replaceFirst("http://", "https://");
            }
        } else {
            previousPageUrl = null;
            nextPageUrl = null;
        }

        if (jsonObject.has("paging") && jsonObject.getJsonObject("paging").has("cursors")) {
            JsonObject jsonCursors = jsonObject.getJsonObject("paging").getJsonObject("cursors");
            beforeCursor = jsonCursors.has("before") ? jsonCursors.getString("before") : null;
            afterCursor = jsonCursors.has("after") ? jsonCursors.getString("after") : null;
        }

        if (jsonObject.has("summary")) {
            JsonObject jsonSummary = jsonObject.getJsonObject("summary");
            totalCount = jsonSummary.has("total_count") ? jsonSummary.getLong("total_count") : null;
        } else {
            totalCount = null;
        }

        this.data = unmodifiableList(dataList);
        this.facebookClient = facebookClient;
        this.connectionType = connectionType;
    }
}
