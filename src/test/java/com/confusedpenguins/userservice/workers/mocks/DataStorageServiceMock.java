package com.confusedpenguins.userservice.workers.mocks;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.confusedpenguins.userservice.data.UserProxy;
import com.confusedpenguins.userservice.workers.DataStorageService;

/**
 * Created by Andrew on 7/12/2017.
 */
public class DataStorageServiceMock extends DataStorageService {
    public DataStorageServiceMock(AmazonDynamoDB dynamoDBClient) {
        super(dynamoDBClient);
    }

    @Override
    public UserProxy saveObject(UserProxy user) {
        return new UserProxy();
    }
}
