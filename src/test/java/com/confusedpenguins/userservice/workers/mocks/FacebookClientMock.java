package com.confusedpenguins.userservice.workers.mocks;

import com.restfb.*;
import com.restfb.batch.BatchRequest;
import com.restfb.batch.BatchResponse;
import com.restfb.exception.devicetoken.FacebookDeviceTokenCodeExpiredException;
import com.restfb.exception.devicetoken.FacebookDeviceTokenDeclinedException;
import com.restfb.exception.devicetoken.FacebookDeviceTokenPendingException;
import com.restfb.exception.devicetoken.FacebookDeviceTokenSlowdownException;
import com.restfb.scope.ScopeBuilder;
import com.restfb.types.DeviceCode;
import com.restfb.types.User;

import java.util.List;
import java.util.Map;

/**
 * Created by Andrew on 7/11/2017.
 */
public class FacebookClientMock implements FacebookClient {
    @Override
    public <T> T fetchObject(String s, Class<T> aClass, Parameter... parameters) {
        if (aClass == User.class) {
            return aClass.cast(new User());
        } else {
            return null;
        }
    }

    @Override
    public <T> T fetchObjects(List<String> list, Class<T> aClass, Parameter... parameters) {
        return null;
    }

    @Override
    public <T> Connection<T> fetchConnection(String s, Class<T> aClass, Parameter... parameters) {
        if (aClass == User.class) {
            return new Connection(this, "{\n" +
                    "  \"data\": [\n" +
                    "  ]\n" +
                    "}", aClass);
        }
        return null;
    }

    @Override
    public <T> Connection<T> fetchConnectionPage(String s, Class<T> aClass) {
        return null;
    }

    @Override
    public <T> List<T> executeQuery(String s, Class<T> aClass, Parameter... parameters) {
        return null;
    }

    @Override
    public <T> T executeMultiquery(Map<String, String> map, Class<T> aClass, Parameter... parameters) {
        return null;
    }

    @Override
    public <T> List<T> executeFqlQuery(String s, Class<T> aClass, Parameter... parameters) {
        return null;
    }

    @Override
    public <T> T executeFqlMultiquery(Map<String, String> map, Class<T> aClass, Parameter... parameters) {
        return null;
    }

    @Override
    public List<BatchResponse> executeBatch(BatchRequest... batchRequests) {
        return null;
    }

    @Override
    public List<BatchResponse> executeBatch(List<BatchRequest> list) {
        return null;
    }

    @Override
    public List<BatchResponse> executeBatch(List<BatchRequest> list, List<BinaryAttachment> list1) {
        return null;
    }

    @Override
    public <T> T publish(String s, Class<T> aClass, Parameter... parameters) {
        return null;
    }

    @Override
    public <T> T publish(String s, Class<T> aClass, List<BinaryAttachment> list, Parameter... parameters) {
        return null;
    }

    @Override
    public <T> T publish(String s, Class<T> aClass, BinaryAttachment binaryAttachment, Parameter... parameters) {
        return null;
    }

    @Override
    public boolean deleteObject(String s, Parameter... parameters) {
        return false;
    }

    @Override
    public List<AccessToken> convertSessionKeysToAccessTokens(String s, String s1, String... strings) {
        return null;
    }

    @Override
    public AccessToken obtainUserAccessToken(String s, String s1, String s2, String s3) {
        return null;
    }

    @Override
    public AccessToken obtainAppAccessToken(String s, String s1) {
        return null;
    }

    @Override
    public AccessToken obtainExtendedAccessToken(String s, String s1, String s2) {
        return null;
    }

    @Override
    public String obtainAppSecretProof(String s, String s1) {
        return null;
    }

    @Override
    public AccessToken obtainExtendedAccessToken(String s, String s1) {
        return null;
    }

    @Override
    public <T> T parseSignedRequest(String s, String s1, Class<T> aClass) {
        return null;
    }

    @Override
    public DeviceCode fetchDeviceCode(String s, ScopeBuilder scopeBuilder) {
        return null;
    }

    @Override
    public DeviceCode fetchDeviceCode(ScopeBuilder scopeBuilder) {
        return null;
    }

    @Override
    public AccessToken obtainDeviceAccessToken(String s, String s1) throws FacebookDeviceTokenCodeExpiredException, FacebookDeviceTokenPendingException, FacebookDeviceTokenDeclinedException, FacebookDeviceTokenSlowdownException {
        return null;
    }

    @Override
    public AccessToken obtainDeviceAccessToken(String s) throws FacebookDeviceTokenCodeExpiredException, FacebookDeviceTokenPendingException, FacebookDeviceTokenDeclinedException, FacebookDeviceTokenSlowdownException {
        return null;
    }

    @Override
    public DebugTokenInfo debugToken(String s) {
        return null;
    }

    @Override
    public JsonMapper getJsonMapper() {
        return new DefaultJsonMapper();
    }

    @Override
    public WebRequestor getWebRequestor() {
        return null;
    }

    @Override
    public String getLogoutUrl(String s) {
        return null;
    }

    @Override
    public String getLoginDialogUrl(String s, String s1, ScopeBuilder scopeBuilder, Parameter... parameters) {
        return null;
    }
}
