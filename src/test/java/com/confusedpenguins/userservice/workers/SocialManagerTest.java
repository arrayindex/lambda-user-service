package com.confusedpenguins.userservice.workers;

import com.confusedpenguins.userservice.data.Data;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 7/8/2017.
 */
public class SocialManagerTest {
    @Test
    public void getFavoriteThing() throws Exception {
        SocialManager socialManager = new SocialManager();
        String beer = "Beer";
        String wine = "Wine";
        String liquor = "Liquor";
        String coffee = "Coffee";

        socialManager.analyzeDataPoint(liquor);
        socialManager.analyzeDataPoint(beer);
        socialManager.analyzeDataPoint(beer);
        socialManager.analyzeDataPoint(wine);
        socialManager.analyzeDataPoint(beer);
        socialManager.analyzeDataPoint(beer);
        socialManager.analyzeDataPoint(beer);
        socialManager.analyzeDataPoint(beer);
        socialManager.analyzeDataPoint(coffee);

        assertEquals("Beer is best", socialManager.getFavoriteThing(), beer);
    }

    @Test
    public void getFavoriteBandTest() throws Exception {
        SocialManager socialManager = new SocialManager();
        Data d1 = new Data();
        d1.setName("foo");
        d1.setFan_count(100);
        socialManager.analyzeDataPoint(d1);

        Data d2 = new Data();
        d2.setName("bar");
        d2.setFan_count(300);
        socialManager.analyzeDataPoint(d2);

        Data d3 = new Data();
        d3.setName("baz");
        d3.setFan_count(100);
        socialManager.analyzeDataPoint(d3);

        assertEquals("bar", socialManager.getFavoriteThing(), "bar");
    }

    @Test
    public void isPopular() throws Exception {
        int totalFriendPopularTest = 51;
        int totalFriendBarelyPopularTest = 50;
        int totalFriendUnPopularTest = 49;
        assertEquals("Under threshold for popular flag", SocialManager.isPopular(totalFriendPopularTest), true);
        assertEquals("Over threshold for popular flag", SocialManager.isPopular(totalFriendUnPopularTest), false);
        assertEquals("On the threshold for popular flag", SocialManager.isPopular(totalFriendBarelyPopularTest), true);
    }
}